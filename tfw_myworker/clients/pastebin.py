
from urllib.parse import urljoin
from urllib.request import urlopen

from bs4 import BeautifulSoup

from tfw_myworker.clients import Client
from tfw_myworker.cache import Cache


class PasteBinClient(Client):
    """
    Client to talk with Paste Bin.
    """
    url = "https://pastebin.com/"
    raw_paste_url = "https://pastebin.com/raw/{key}"
    cache = Cache()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.archived_pasted = urljoin(self.url, '/archive')

    def fetch_pastes(self):
        """
        Fetch all recent pastes from pastebin.

        Return:
            (list): list if dictionaries containing 'key', 'title' and 'url' for raw paste content.
        """
        pastes = self.cache.get('archive')
        if pastes:
            return pastes

        pastes = []

        with urlopen(self.archived_pasted) as response:
            content = response.read().decode('utf-8')

            html = BeautifulSoup(content)

            for anchor in html.select('table.maintable td a'):
                pastes.append({
                    'key': anchor.get('href').lstrip('/'),
                    'title': anchor.text,
                    'url': self.raw_paste_url.format(key=anchor.get('href').lstrip('/'))
                })

        self.cache.set('archive', pastes)
        self.cache.stash()
        return pastes
