"""
Base client to talk with services.
"""
from urllib.request import urlopen


class Client(object):
    url = None

    @staticmethod
    def fetch_file(url):
        """
        Fetch the file with the given urls and return its contents.
        """
        with urlopen(url) as file:
            return file.read().decode('utf-8')
