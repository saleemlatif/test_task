import json

from tfw_myworker.clients import Client
from urllib.parse import urljoin
from urllib.request import urlopen
from tfw_myworker.workers.utils import update_query


class GithubClient(Client):
    """
    Client to talk with github.
    """
    url = "https://api.github.com/"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.gists_url = urljoin(self.url, '/gists')

    def fetch_gists(self, page=1):
        """
        Fetch gists from Github.
        """
        with urlopen(update_query(self.gists_url, page=page)) as gists_response:
            content = gists_response.read().decode('utf-8')
            return json.loads(content)

