"""
Exception definitions.
"""


class LimitExceeded(Exception):
    """
    Exception to raise when enforced limit is exceeded.
    """


class WorkerNotFound(Exception):
    """
    Exception to raise when worker can not be found.
    """
