from tfw_myworker.workers import Worker
from tfw_myworker.clients import PasteBinClient
from tfw_myworker.response import ResponseCode, WorkerResponse
from tfw_myworker.errors import LimitExceeded


class PasteBinWorker(Worker):
    """
    PasteBin worker.
    """
    name = "PasteBin Worker"
    client = PasteBinClient()

    visited_pastes = 0

    def run(self):
        """
        Run the worker.
        """
        super().run()

        response = WorkerResponse()
        response.response_code = ResponseCode.FAILURE
        response.data = {}
        
        while True:
            self.log("Fetching pasts from pastebin.")
            pastes = self.client.fetch_pastes()
            try:
                match = self.search_patterns(pastes)
            except LimitExceeded:
                break

            if match:
                response.data = {
                    'url': match
                }
                response.response_code = ResponseCode.SUCCESS
                break

        return response

    def search_patterns(self, pastes: list):
        for paste in pastes:
            url = paste['url']

            if self.visited_pastes > self.limit:
                self.log("Limit '{}' exceeded.".format(self.limit))
                raise LimitExceeded("Limit '{}' exceeded.".format(self.limit))

            if self.match_file(url):
                self.log("Paste with matching content found.")
                self.log(url)
                return url

            self.visited_pastes += 1

    def match_file(self, file: str):
        self.log("Fetching file: {}".format(file))
        content = self.client.fetch_file(file)

        if any(map(lambda regex: bool(regex.findall(content)), self.compiled_patterns)):
            return file
        return None
