
from tfw_myworker.workers import Worker
from tfw_myworker.response import ResponseCode, WorkerResponse
from tfw_myworker.clients import GithubClient
from tfw_myworker.errors import LimitExceeded


class GithubGistWorker(Worker):
    """
    GithubGist worker.
    """
    name = "GithubGist Worker"
    client = GithubClient()

    visited_gists = 0

    def run(self):
        """
        Run the worker.
        """
        super().run()

        response = WorkerResponse()
        response.response_code = ResponseCode.FAILURE
        response.data = {}

        page = 0
        while True:
            page += 1

            self.log("Fetching page={} of gists from Github.".format(page))
            gists = self.client.fetch_gists()
            try:
                match = self.search_patterns(gists)
            except LimitExceeded:
                break

            if match:
                response.data = {
                    'url': match
                }
                response.response_code = ResponseCode.SUCCESS
                break

        return response

    def search_patterns(self, gists: list):
        for gist in gists:
            url = gist['url']

            if self.visited_gists > self.limit:
                self.log("Gist limit '{}' exceeded.".format(self.limit))
                raise LimitExceeded("Gist limit '{}' exceeded.".format(self.limit))

            if self.match_files(gist['files']):
                self.log("Gist with matching content found.")
                self.log(url)
                return url

            self.visited_gists += 1

    def match_files(self, files: dict):
        for name, data in files.items():
            self.log("Fetching file: {}".format(data['raw_url']))
            content = self.client.fetch_file(data['raw_url'])

            if any(map(lambda regex: bool(regex.findall(content)), self.compiled_patterns)):
                return name
        return None
