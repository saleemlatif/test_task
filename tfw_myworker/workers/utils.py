"""
Utility functions.
"""
import urllib.parse as urlparse
from urllib.parse import urlencode


def update_query(url: str, **params):
    """
    Update query parameters of the given url.
    """
    url_parts = list(urlparse.urlparse(url))
    query = dict(urlparse.parse_qsl(url_parts[4]))
    query.update(params)

    url_parts[4] = urlencode(query)

    return urlparse.urlunparse(url_parts)
