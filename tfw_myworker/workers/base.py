import logging
import re

log = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)


class Worker(object):
    """
    Worker base class to handle common worker operations.
    """
    name = 'My Worker'

    def __init__(self, limit: int, patterns: list):
        self.limit = limit
        self.patterns = patterns
        self.compiled_patterns = list(map(re.compile, patterns))

    def run(self):
        """
        Run worker.
        """
        self.log("Worker run started.")

    def log(self, message: str):
        """
        Log worker message.
        """
        log.info("\033[92m {self.name}: \033[00m | {message}".format(self=self, message=message))
