from tfw_myworker.workers import Worker
from tfw_myworker.errors import WorkerNotFound


def get_worker(name: str):
    """
    Find and return the worker class that matches the given name.

    Exceptions:
        (WorkerNotFound): Raised if no worker exists with the given name.
    """
    for cls in get_all_worker_classes(Worker):
        if cls.name == name:
            return cls
    else:
        raise WorkerNotFound("No worker with name '{}'".format(name))


def get_all_worker_classes(base: object = Worker):
    """
    Scan workers/ package to get a list of all worker classes.

    Return:
        (set): Returns a set of all the defined subclasses of the given base class.
    """
    subclasses = set()

    for child in base.__subclasses__():
        if child not in subclasses:
            subclasses.add(child)

    return subclasses
