"""
Module to handle caching.
"""
import json

from pathlib import Path


class Cache(object):
    """
    A minimalist cache implementation.
    """
    __cache_file = str(Path(__file__).parent.parent / "response.json.cache")
    __cache = {}

    def __init__(self):
        """
        Initialize cache from file.
        """
        try:
            self.__cache = json.load(open(self.__cache_file))
        except FileNotFoundError:
            pass

    def get(self, key):
        """
        Return the cached content by key.
        """
        if key in self.__cache:
            return self.__cache[key]

    def set(self, key: str, value: dict or object):
        """
        Update the cache for the given key.
        """
        self.__cache[key] = value

    def stash(self):
        """
        Stash the cache on to the file.
        """
        json.dump(self.__cache, open(self.__cache_file, 'w+'))
