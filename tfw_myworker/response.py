from enum import Enum


class ResponseCode(Enum):
    """
    Response Code
    """
    SUCCESS = 1
    FAILURE = 0


class WorkerResponse(object):
    response_code = None
    data = None
