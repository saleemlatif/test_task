"""
Tests for utility functions.
"""

import unittest
from tfw_myworker import utils
from tfw_myworker.workers import Worker, GithubGistWorker, PasteBinWorker
from tfw_myworker.errors import WorkerNotFound


class TestUtils(unittest.TestCase):
    """
    Utility functions tests.
    """

    def test_get_all_worker_classes(self):
        """
        Verify that `get_all_worker_classes` works as expected.
        """
        self.assertSetEqual(
            utils.get_all_worker_classes(Worker),
            {GithubGistWorker, PasteBinWorker}
        )

    def test_get_worker(self):
        """
        Verify that `get_worker` works as expected.
        """
        self.assertEqual(
            utils.get_worker("GithubGist Worker"),
            GithubGistWorker,
        )
        self.assertEqual(
            utils.get_worker("PasteBin Worker"),
            PasteBinWorker,
        )

        with self.assertRaises(WorkerNotFound):
            utils.get_worker("Undefined Worker")
