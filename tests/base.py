"""
Functionality common to all test cases.
"""

import unittest
from pathlib import Path

from tfw_myworker.clients import base


class TestCase(unittest.TestCase):
    """
    Base Test Case.
    """

    def setUp(self):
        """
        Set up mocking for all tests.
        """
        super().setUp()

        self.test_file = str(Path(__file__).parent / "test_files/contents.md")
        base.urlopen = self._get_mock_urlopen(self.test_file)

    def _get_mock_urlopen(self, response_file):
        """
        Get mock urlopen for tests.
        """
        def urlopen(*args, **kwargs):
            req = open(response_file, 'rb')
            self.addCleanup(req.close)
            return req
        return urlopen
