vlc-nightly v3.0.7.20190518 - Passed - Package Test Results
 * [https://chocolatey.org/packages/vlc-nightly/3.0.7.20190518](https://chocolatey.org/packages/vlc-nightly/3.0.7.20190518)
 * Tested 18 May 2019 09:23:38 +00:00
 * Tested against win2012r2x64 (Windows Server 2012 R2 x64)
 * Tested with the latest version of choco, possibly a beta version.
 * Tested with chocolatey-package-verifier service v0.4.0-38-g3187e94
 * Install was successful.
 * Uninstall was successful.