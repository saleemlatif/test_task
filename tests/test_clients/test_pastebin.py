"""
Tests for Pastebin client.
"""
from pathlib import Path

from unittest.mock import Mock
from tests import TestCase
from tfw_myworker.clients import pastebin

from tfw_myworker.clients import PasteBinClient


class TestPasteBinClient(TestCase):
    """
    Tests for PasteBin client.
    """

    def setUp(self):
        """
        Setup client instance and mocking.
        """
        super().setUp()

        self.test_pastes = str(Path(__file__).parent.parent / "test_files/pastebin.html")
        self.client = PasteBinClient()
        pastebin.urlopen = self._get_mock_urlopen(self.test_pastes)

        # disable cache
        self.client.cache = Mock(get=Mock(return_value=None))

        self.expected = [{'title': 'Untitled', 'url': 'https://pastebin.com/raw/dhdkwaTv', 'key': 'dhdkwaTv'}, {'title': 'Untitled', 'url': 'https://pastebin.com/raw/kEScN9HN', 'key': 'kEScN9HN'}, {'title': 'Untitled', 'url': 'https://pastebin.com/raw/jk6FX3uN', 'key': 'jk6FX3uN'}, {'title': 'fattt updated updated updated', 'url': 'https://pastebin.com/raw/jFPc9Cyc', 'key': 'jFPc9Cyc'}, {'title': 'some shit gui', 'url': 'https://pastebin.com/raw/Zwvgh7xt', 'key': 'Zwvgh7xt'}, {'title': 'orgate', 'url': 'https://pastebin.com/raw/h5bESY1m', 'key': 'h5bESY1m'}, {'title': 'Lua', 'url': 'https://pastebin.com/raw/archive/lua', 'key': 'archive/lua'}, {'title': 'zzlatev', 'url': 'https://pastebin.com/raw/vP5k7gnN', 'key': 'vP5k7gnN'}, {'title': 'nyc executor', 'url': 'https://pastebin.com/raw/nPnHbYaS', 'key': 'nPnHbYaS'}, {'title': 'Untitled', 'url': 'https://pastebin.com/raw/WEJeLDEb', 'key': 'WEJeLDEb'}, {'title': 'Untitled', 'url': 'https://pastebin.com/raw/NQQ3mZRm', 'key': 'NQQ3mZRm'}, {'title': 'Minrcraft', 'url': 'https://pastebin.com/raw/fERAKhYh', 'key': 'fERAKhYh'}, {'title': 'Untitled', 'url': 'https://pastebin.com/raw/0FF4XH7B', 'key': '0FF4XH7B'}, {'title': 'snake', 'url': 'https://pastebin.com/raw/55vXydX0', 'key': '55vXydX0'}, {'title': 'Untitled', 'url': 'https://pastebin.com/raw/q0DMuh3i', 'key': 'q0DMuh3i'}, {'title': 'Untitled', 'url': 'https://pastebin.com/raw/yye9YLmS', 'key': 'yye9YLmS'}, {'title': 'C#', 'url': 'https://pastebin.com/raw/archive/csharp', 'key': 'archive/csharp'}, {'title': 'Koh lanta GdC épisode 10', 'url': 'https://pastebin.com/raw/GymU6R9n', 'key': 'GymU6R9n'}, {'title': 'TH.M3U', 'url': 'https://pastebin.com/raw/20WtLLqV', 'key': '20WtLLqV'}, {'title': 'Untitled', 'url': 'https://pastebin.com/raw/vgsPg3vm', 'key': 'vgsPg3vm'}, {'title': 'Untitled', 'url': 'https://pastebin.com/raw/a1gRfMau', 'key': 'a1gRfMau'}, {'title': 'Superlopez (2018)', 'url': 'https://pastebin.com/raw/XFVuLfc2', 'key': 'XFVuLfc2'}, {'title': 'Untitled', 'url': 'https://pastebin.com/raw/Deg0dGHC', 'key': 'Deg0dGHC'}, {'title': 'CAR RENTAL', 'url': 'https://pastebin.com/raw/0inY3mKa', 'key': '0inY3mKa'}, {'title': 'Untitled', 'url': 'https://pastebin.com/raw/bKUAGWd6', 'key': 'bKUAGWd6'}, {'title': 'Python', 'url': 'https://pastebin.com/raw/archive/python', 'key': 'archive/python'}, {'title': 'nyc voltorb', 'url': 'https://pastebin.com/raw/RL1U1g5G', 'key': 'RL1U1g5G'}, {'title': 'Untitled', 'url': 'https://pastebin.com/raw/SQ8X2cqe', 'key': 'SQ8X2cqe'}, {'title': 'aa', 'url': 'https://pastebin.com/raw/id9sJbtS', 'key': 'id9sJbtS'}, {'title': 'lajme gjenerale dhe sporte me 12 kanale shqiptare', 'url': 'https://pastebin.com/raw/SKZMQb23', 'key': 'SKZMQb23'}, {'title': 'Untitled', 'url': 'https://pastebin.com/raw/Ppw1syJA', 'key': 'Ppw1syJA'}, {'title': 'Mulheres Maduras', 'url': 'https://pastebin.com/raw/yaKGB1EJ', 'key': 'yaKGB1EJ'}, {'title': 'dpost 2019-05-18 12:02:10', 'url': 'https://pastebin.com/raw/SaLDhceb', 'key': 'SaLDhceb'}, {'title': 'Untitled', 'url': 'https://pastebin.com/raw/ciHtEVWC', 'key': 'ciHtEVWC'}, {'title': 'url', 'url': 'https://pastebin.com/raw/vd0zDUXt', 'key': 'vd0zDUXt'}, {'title': 'Untitled', 'url': 'https://pastebin.com/raw/yRPNr0rW', 'key': 'yRPNr0rW'}, {'title': 'Java', 'url': 'https://pastebin.com/raw/archive/java', 'key': 'archive/java'}, {'title': 'SK v 0.10.1.9 Log Level 6', 'url': 'https://pastebin.com/raw/SgmGVZ7q', 'key': 'SgmGVZ7q'}, {'title': 'Untitled', 'url': 'https://pastebin.com/raw/7DEnap2k', 'key': '7DEnap2k'}, {'title': 'Untitled', 'url': 'https://pastebin.com/raw/6Q3UVzWs', 'key': '6Q3UVzWs'}, {'title': 'Untitled', 'url': 'https://pastebin.com/raw/xRLvd1R5', 'key': 'xRLvd1R5'}, {'title': 'Untitled', 'url': 'https://pastebin.com/raw/eMKWsXCE', 'key': 'eMKWsXCE'}, {'title': 'Untitled', 'url': 'https://pastebin.com/raw/fJ83gVek', 'key': 'fJ83gVek'}, {'title': 'dinj 2019-05-18 12:02:10', 'url': 'https://pastebin.com/raw/YHavDmhh', 'key': 'YHavDmhh'}, {'title': 'Untitled', 'url': 'https://pastebin.com/raw/L6qgYmCa', 'key': 'L6qgYmCa'}, {'title': 'Untitled', 'url': 'https://pastebin.com/raw/84kknKuZ', 'key': '84kknKuZ'}, {'title': 'UK.M3U', 'url': 'https://pastebin.com/raw/3zAS70jE', 'key': '3zAS70jE'}, {'title': 'Untitled', 'url': 'https://pastebin.com/raw/ZpNJ7KVh', 'key': 'ZpNJ7KVh'}, {'title': 'Untitled', 'url': 'https://pastebin.com/raw/cR14h66N', 'key': 'cR14h66N'}, {'title': 'With love from Karen', 'url': 'https://pastebin.com/raw/LLZKVYWB', 'key': 'LLZKVYWB'}, {'title': 'Untitled', 'url': 'https://pastebin.com/raw/8gFcQy50', 'key': '8gFcQy50'}, {'title': 'Untitled', 'url': 'https://pastebin.com/raw/cqHQF8tY', 'key': 'cqHQF8tY'}, {'title': 'Untitled', 'url': 'https://pastebin.com/raw/RKQcrSdf', 'key': 'RKQcrSdf'}]

    def test_fetch_pastes(self):
        """
        Verify `fetch_pastes` on Github Client works as expected.
        """
        response = self.client.fetch_pastes()
        self.assertListEqual(response, self.expected)

    def test_fetch_file(self):
        """
        Verify `fetch_file` on base Client works as expected.
        """
        expected = open(self.test_file).read()
        response = self.client.fetch_file("contents.md")

        self.assertEqual(response, expected)
