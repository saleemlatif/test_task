"""
Tests for Github client.
"""
import json
from pathlib import Path

from tests import TestCase
from tfw_myworker.clients import github

from tfw_myworker.clients import GithubClient


class TestGithubClient(TestCase):
    """
    Tests for Github client.
    """

    def setUp(self):
        """
        Setup client instance and mocking.
        """
        super().setUp()

        self.test_gists = str(Path(__file__).parent.parent / "test_files/response.json")
        self.client = GithubClient()
        github.urlopen = self._get_mock_urlopen(self.test_gists)

    def test_fetch_gists(self):
        """
        Verify `fetch_gists` on Github Client works as expected.
        """
        expected = json.load(open(self.test_gists))
        response = self.client.fetch_gists()

        self.assertListEqual(response, expected)

    def test_fetch_file(self):
        """
        Verify `fetch_file` on base Client works as expected.
        """
        expected = open(self.test_file).read()
        response = self.client.fetch_file("contents.md")

        self.assertEqual(response, expected)
