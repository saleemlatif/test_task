"""
Test Github Gist Worker.
"""
from pathlib import Path

from tests import TestCase
from tfw_myworker.clients import github
from tfw_myworker.workers import GithubGistWorker
from tfw_myworker.response import ResponseCode


class TestGithubGistWorker(TestCase):
    """
    Test Github Gist Worker.
    """

    def setUp(self):
        """
        Set up Github Gist worker class instance.
        """
        super().setUp()
        self.test_gists = str(Path(__file__).parent.parent / "test_files/response.json")
        github.urlopen = self._get_mock_urlopen(self.test_gists)

        self.worker = GithubGistWorker(
            limit=1000,
            patterns=[r"Uninstall was successful"]
        )

    def test_run(self):
        """
        Verify that worker runs as expected.
        """
        response = self.worker.run()

        assert response is not None
        assert response.response_code == ResponseCode.SUCCESS
        self.assertDictEqual(
            response.data,
            {
                "url": "https://api.github.com/gists/7c2cb4c910ab0f071bf63c4509ff6322"
            },
        )

    def test_limit_exceeded_run(self):
        """
        Verify that Worker exits gracefully if no match is found.
        """
        worker = GithubGistWorker(
            limit=10,
            patterns=[r"Unknown Pattern that is not existent in the file."]
        )
        response = worker.run()
        assert response is not None
        assert response.response_code == ResponseCode.FAILURE
        assert response.data == {}
