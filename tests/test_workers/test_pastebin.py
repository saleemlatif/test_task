"""
Test PasteBin Worker.
"""
from pathlib import Path
from unittest.mock import Mock

from tests import TestCase
from tfw_myworker.clients import pastebin
from tfw_myworker.workers import PasteBinWorker
from tfw_myworker.response import ResponseCode


class TestPasteBinWorker(TestCase):
    """
    Test Paste Bin Worker.
    """

    def setUp(self):
        """
        Set up PasteBin worker class instance.
        """
        super().setUp()
        self.test_pastes = str(Path(__file__).parent.parent / "test_files/pastebin.html")
        pastebin.urlopen = self._get_mock_urlopen(self.test_pastes)

        self.worker = PasteBinWorker(
            limit=1000,
            patterns=[r"Uninstall was successful"]
        )
        # disable cache
        self.worker.client.cache = Mock(get=Mock(return_value=None))

    def test_run(self):
        """
        Verify that worker runs as expected.
        """
        response = self.worker.run()

        assert response is not None
        assert response.response_code == ResponseCode.SUCCESS
        self.assertDictEqual(
            response.data,
            {
                "url": "https://pastebin.com/raw/dhdkwaTv"
            },
        )

    def test_limit_exceeded_run(self):
        """
        Verify that Worker exits gracefully if no match is found.
        """
        worker = PasteBinWorker(
            limit=10,
            patterns=[r"Unknown Pattern that is not existent in the file."]
        )
        response = worker.run()
        assert response is not None
        assert response.response_code == ResponseCode.FAILURE
        assert response.data == {}
