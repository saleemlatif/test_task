Project Structure:
------------------

Project contains the following files and directories.
1. `tfw_myworker/`
	> Directory to hold all the workers and client for the project. This is the main 
	directory that contains all of the relevant code.
	1. `cache.py`
		> Code for caching network data.
	2. `errors.py`
		> Custom exceptions to be used throughout the project.
	3. `response.py`
		> Utility classes for response.
	4. `utils.py`
		> Utility functions.
	5. `clients/`
		> Directory to hold all the clients, clients communicate with their corresponding service. files in this directory are self explanatory.
	6. `workers/`
		> Directory to hold all the workers, workers handle data processing and pattern matching. files in this directory are self explanatory.
2. `tests/`
	> Directory to hold all the tests. You can run tests using `python -m unittest`. 
	Sub directories are pretty much self explanatory and contains test for each file.

Pre-Requisites:
---------------
Code is written in `Python 3` and the only requirement to run the code is `beautifulsoup4`. Other than that, I have written the functionality 
without using any third party packages and using only the features python provides natively.

How to Run?
-----------
Make these packages importable in your code and then do the following
```python
# For paste bin
from tfw_myworker.workers import PasteBinWorker
worker = PasteBinWorker(
	limit=100, 
	patterns=[r"RegistroUtenti"],
)
response = worker.run()
print(response.data)

# For github gists

from tfw_myworker.workers import GithubGistWorker
worker = GithubGistWorker(
	limit=100, 
	patterns=[r"RegistroUtenti"],
)
response = worker.run()
print(response.data)
```

How to Test?
------------
You can run the test by running the following command inside the project directory
```shell
python -m unittest -v
```